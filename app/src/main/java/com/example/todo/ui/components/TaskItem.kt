package com.example.todo.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.DismissValue
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SwipeToDismiss
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDismissState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.todo.data.DataSource
import com.example.todo.data.TaskPriority
import com.example.todo.data.Todo
import com.example.todo.ui.theme.TodoTheme
import com.example.todo.ui.utils.DismissBackground

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TaskItem(
    todo: Todo,
    onDoneTask: (Int, Boolean) -> Unit,
    onDeleteTask: (Todo) -> Unit
) {
    val currentItem by rememberUpdatedState(todo)

    val dismissState = rememberDismissState(
        confirmValueChange = { dismissValue ->
            when (dismissValue) {
                DismissValue.DismissedToEnd -> {
                    onDeleteTask(currentItem)
                    false
                }

                DismissValue.DismissedToStart -> {
                    onDoneTask(currentItem.id, !currentItem.isDone)
                    false
                }

                else -> false
            }
        }, positionalThreshold = { 150.dp.toPx() }
    )

    SwipeToDismiss(
        state = dismissState,
        background = { DismissBackground(dismissState = dismissState) },
        modifier = Modifier
            .wrapContentSize()
            .padding(8.dp),
        dismissContent = {
            ElevatedCard(
                elevation = CardDefaults.cardElevation(defaultElevation = 4.dp),
                colors = CardDefaults.elevatedCardColors(containerColor = MaterialTheme.colorScheme.onPrimary),
                shape = CardDefaults.elevatedShape,
                modifier = Modifier
                    .wrapContentHeight()
                    .fillMaxWidth()
            ) {
                Row(
                    verticalAlignment = Alignment.Top,
                    horizontalArrangement = Arrangement.Start,
                    modifier = Modifier
                        .padding(vertical = 16.dp, horizontal = 16.dp)
                        .wrapContentSize()
                ) {
                    Checkbox(
                        checked = currentItem.isDone,
                        onCheckedChange = { onDoneTask(currentItem.id, it) },
                        modifier = Modifier.size(24.dp)
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    Column(
                        modifier = Modifier.wrapContentSize(),
                        verticalArrangement = Arrangement.Top,
                        horizontalAlignment = Alignment.Start
                    ) {
                        TaskTitle(
                            taskName = currentItem.taskName,
                            taskPriority = currentItem.taskPriority
                        )
                        TaskDescription(taskDescription = currentItem.taskDescription)
                        TaskTime(currentItem.taskDate)
                    }
                }
            }
        }
    )
}

@Composable
fun TaskTitle(taskName: String, taskPriority: TaskPriority) {
    val priorityBackgroundColor: Color = when (taskPriority) {
        TaskPriority.Low -> MaterialTheme.colorScheme.background
        TaskPriority.Medium -> MaterialTheme.colorScheme.surfaceVariant
        TaskPriority.High -> MaterialTheme.colorScheme.errorContainer
    }

    val priorityLabelColor: Color = when (taskPriority) {
        TaskPriority.Low -> MaterialTheme.colorScheme.onBackground
        TaskPriority.Medium -> MaterialTheme.colorScheme.onSurfaceVariant
        TaskPriority.High -> MaterialTheme.colorScheme.onErrorContainer
    }

    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .wrapContentHeight()
            .fillMaxWidth()
    ) {
        Text(
            text = taskName,
            modifier = Modifier
                .padding(horizontal = 8.dp),
            style = TextStyle(
                fontSize = 16.sp,
                fontFamily = FontFamily.SansSerif,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.onBackground
            )
        )
        Box(
            modifier = Modifier
                .clip(RoundedCornerShape(4.dp))
                .wrapContentSize()
        ) {
            Text(
                text = taskPriority.name,
                modifier = Modifier
                    .background(priorityBackgroundColor)
                    .padding(horizontal = 8.dp, vertical = 2.dp),
                style = TextStyle(
                    fontSize = 12.sp,
                    textAlign = TextAlign.Start,
                    color = priorityLabelColor
                ),
            )
        }
    }
}

@Composable
fun TaskDescription(taskDescription: String) {
    Text(
        text = taskDescription,
        maxLines = 3,
        overflow = TextOverflow.Ellipsis,
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .wrapContentHeight()
            .fillMaxWidth(),
        style = TextStyle(
            fontSize = 14.sp,
            fontFamily = FontFamily.SansSerif,
            fontWeight = FontWeight.Normal,
            color = MaterialTheme.colorScheme.onBackground
        )
    )
}


@Composable
fun TaskTime(taskCreatedAt: String) {
    Text(
        text = "In $taskCreatedAt",
        modifier = Modifier
            .padding(horizontal = 8.dp)
            .wrapContentHeight()
            .fillMaxWidth(),
        style = TextStyle(
            fontSize = 12.sp,
            fontFamily = FontFamily.SansSerif,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.onBackground
        )
    )
}

@Preview(showBackground = true)
@Composable
fun TaskItemPreview() {
    TodoTheme {
        TaskItem(/*Preview*/
            todo = DataSource.todos[0],
            onDoneTask = { _, _ -> }
        ) {}
    }
}