package com.example.todo.ui.components.dialog

import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.res.stringResource
import com.example.todo.R
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TodoDatePickerDialog(
    onDateChanged: (String) -> Unit,
    onDismissDialog: () -> Unit
) {
    val datePickerState = rememberDatePickerState()
    val dateFormat = rememberSaveable {
        SimpleDateFormat(
            "EEE MMM dd HH:mm:ss 'GMT'Z yyyy",
            Locale.getDefault()
        )
    }
    val currentDateAndTime = Calendar.getInstance().timeInMillis

    DatePickerDialog(
        onDismissRequest = { /*TODO*/ },
        confirmButton = {
            TextButton(
                onClick = {
                    val selectedDate = Calendar.getInstance().apply {
                        timeInMillis = datePickerState.selectedDateMillis ?: currentDateAndTime
                    }
                    if (selectedDate.after(Calendar.getInstance())) {
                        val outputFormat = SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH)

                        val parsedDate = dateFormat.parse(selectedDate.time.toString())
                        val formattedDate = outputFormat.format(parsedDate ?: "")

                        onDateChanged(formattedDate)
                    }
                }
            ) {
                Text(text = stringResource(id = R.string.ok))
            }
        },
        dismissButton = {
            TextButton(onClick = onDismissDialog) {
                Text(text = stringResource(id = R.string.cancel))
            }
        }
    ) {
        DatePicker(state = datePickerState)
    }
}