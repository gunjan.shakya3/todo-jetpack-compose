package com.example.todo.ui.screens

import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowForward
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.todo.R
import com.example.todo.data.TaskPriority
import com.example.todo.data.Todo
import com.example.todo.ui.components.CategoryDropdown
import com.example.todo.ui.components.dialog.TodoDatePickerDialog
import com.example.todo.ui.components.dialog.TodoTimePickerDialog
import com.example.todo.ui.components.textfield.DateTextField
import com.example.todo.ui.components.textfield.DescriptionTextField
import com.example.todo.ui.components.textfield.TaskTextField
import com.example.todo.ui.components.textfield.TimeTextField
import com.example.todo.ui.theme.TodoTheme

@Composable
fun CreateNewTaskScreen(
    modifier: Modifier = Modifier,
    onCreateTask: (Todo) -> Unit
) {
    val context = LocalContext.current

    var taskName by rememberSaveable { mutableStateOf("") }
    var description by rememberSaveable { mutableStateOf("") }
    var priority by rememberSaveable { mutableStateOf(TaskPriority.Low) }
    var date by rememberSaveable { mutableStateOf("") }
    var time by rememberSaveable { mutableStateOf("") }

    var showDatePicker by rememberSaveable { mutableStateOf(false) }
    var showTimePicker by rememberSaveable { mutableStateOf(false) }

    Box(modifier.fillMaxSize()) {
        Column(
            modifier = Modifier
                .wrapContentSize()
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Top
        ) {
            TaskTextField(
                value = taskName,
                onValueChanged = { taskName = it }
            )
            DescriptionTextField(
                value = description,
                onValueChanged = { description = it }
            )
            Row(modifier = Modifier.padding(horizontal = 16.dp)) {
                DateTextField(
                    value = date, onValueChanged = { date = it }, modifier = Modifier
                        .weight(1f)
                        .padding(end = 8.dp)
                ) { showDatePicker = true }
                TimeTextField(
                    value = time, onValueChanged = { time = it }, modifier = Modifier
                        .weight(1f)
                        .padding(start = 8.dp)
                ) { showTimePicker = true }
            }
            CategoryDropdown(
                selectedValue = priority.name,
                onValueChangedEvent = { priority = it }
            )
        }

        FloatingActionButton(
            onClick = {
                if (taskName.isEmpty() && description.isEmpty()) {
                    Toast.makeText(
                        context,
                        context.getText(R.string.fill_required_fields),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    onCreateTask(
                        Todo(
                            taskName = taskName,
                            taskDescription = description,
                            taskPriority = priority,
                            taskDate = "$date $time",
                        )
                    )
                }
            },
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .padding(end = 16.dp, bottom = 32.dp)
        ) {
            Icon(
                imageVector = Icons.AutoMirrored.Filled.ArrowForward,
                contentDescription = stringResource(id = R.string.add_task),
            )
        }

        if (showDatePicker) {
            TodoDatePickerDialog(
                onDateChanged = {
                    date = it
                    showDatePicker = false
                },
                onDismissDialog = { showDatePicker = false }
            )
        }

        if (showTimePicker) {
            TodoTimePickerDialog(
                onDismissRequest = { /*TODO*/ },
                onConfirmButtonPressed = {
                    time = it
                    showTimePicker = false
                },
                onDismissButtonPressed = { showDatePicker = false }
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CreateNewTaskScreenPreview() {
    TodoTheme {
        CreateNewTaskScreen(onCreateTask = {})
    }
}