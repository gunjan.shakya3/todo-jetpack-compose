package com.example.todo.ui.components

import androidx.annotation.StringRes
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.todo.R
import com.example.todo.ui.theme.TodoTheme

@Composable
fun NegativeButton(
    @StringRes label: Int,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    OutlinedButton(
        modifier = modifier.padding(horizontal = 8.dp),
        onClick = { onClick() },
        border = BorderStroke(
            width = 1.dp,
            color = MaterialTheme.colorScheme.primary
        ),
        shape = RoundedCornerShape(8.dp)
    ) {
        Text(text = stringResource(id = label))
    }
}

@Composable
fun PositiveButton(
    @StringRes label: Int,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Button(
        modifier = modifier
            .padding(horizontal = 8.dp),
        onClick = { onClick() },
        shape = RoundedCornerShape(8.dp)
    ) {
        Text(text = stringResource(id = label))
    }
}

@Composable
fun FormButton(
    modifier: Modifier = Modifier,
    onCreateTaskPressed: () -> Unit,
    onCancelPressed: () -> Unit
) {
    Row(
        modifier = modifier
            .padding(8.dp)
    ) {
        NegativeButton(
            label = R.string.cancel,
            onClick = { onCancelPressed() },
            modifier = Modifier
                .fillMaxWidth()
                .height(48.dp)
                .weight(1f)
        )
        PositiveButton(
            label = R.string.create_task,
            onClick = { onCreateTaskPressed() },
            modifier = Modifier
                .fillMaxWidth()
                .height(48.dp)
                .weight(1f)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun FormButtonPreview(){
    TodoTheme {
        FormButton(onCreateTaskPressed = { /*TODO*/ }) {
            
        }
    }
}