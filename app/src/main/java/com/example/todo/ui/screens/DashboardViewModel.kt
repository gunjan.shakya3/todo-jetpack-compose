package com.example.todo.ui.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todo.data.TaskPriority
import com.example.todo.data.Todo
import com.example.todo.data.TodoDAO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DashboardViewModel(private val todoDAO: TodoDAO) : ViewModel() {
    fun upsertTodo(todo: Todo) {
        viewModelScope.launch(Dispatchers.IO) {
            todoDAO.upsertTodo(todo = todo)
        }
    }

    fun doneTodo(todoId: Int, isDone: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            todoDAO.doneTodo(todoId = todoId, isDone = isDone)
        }
    }

    fun deleteTodo(todoId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            todoDAO.deleteTodo(todoId)
        }
    }

    fun getAllTodos() = todoDAO.getAllTodos()

    fun sortByPriority(typePriority: TaskPriority) {
        todoDAO.sortByPriority(typePriority.name)
    }
}