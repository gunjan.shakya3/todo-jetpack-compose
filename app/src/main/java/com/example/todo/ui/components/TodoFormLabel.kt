package com.example.todo.ui.components

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.example.todo.R
import com.example.todo.ui.theme.TodoTheme

@Composable
fun TodoFormLabel(
    modifier: Modifier = Modifier,
    @StringRes label: Int,
    textStyle: TextStyle = TextStyle(
        fontSize = 14.sp,
        fontFamily = FontFamily.SansSerif,
        fontWeight = FontWeight.Bold,
        color = MaterialTheme.colorScheme.onBackground
    )
) {
    Text(
        text = stringResource(id = label),
        style = textStyle,
        modifier = modifier
            .fillMaxWidth()
    )
}

@Preview(showBackground = true)
@Composable
fun TodoFormLabelPreview() {
    TodoTheme {
        TodoFormLabel(label = R.string.task)
    }
}