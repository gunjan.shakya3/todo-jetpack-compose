package com.example.todo.ui.components.textfield

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.todo.R
import com.example.todo.ui.components.TodoFormLabel
import com.example.todo.ui.theme.TodoTheme

@Composable
fun DateTextField(
    modifier: Modifier = Modifier,
    value: String,
    onValueChanged: (String) -> Unit,
    clickable: () -> Unit = {}
) {
    Column(modifier = modifier) {
        TodoFormLabel(label = R.string.date)
        Spacer(modifier = Modifier.height(8.dp))
        TextField(
            value = value,
            onValueChange = { onValueChanged(it) },
            shape = RoundedCornerShape(8.dp),
            enabled = false,
            placeholder = {
                Text(
                    text = stringResource(id = R.string.date_format),
                    style = TextStyle(
                        fontSize = 12.sp,
                        fontFamily = FontFamily.SansSerif,
                        fontWeight = FontWeight.Normal,
                        color = MaterialTheme.colorScheme.onSecondaryContainer
                    )
                )
            },
            colors = TextFieldDefaults.colors(
                focusedContainerColor = MaterialTheme.colorScheme.primaryContainer,
                focusedIndicatorColor = Color.Transparent,
                unfocusedContainerColor = MaterialTheme.colorScheme.surfaceVariant,
                unfocusedIndicatorColor = Color.Transparent,
                disabledContainerColor = MaterialTheme.colorScheme.surfaceVariant,
                disabledIndicatorColor = Color.Transparent,
                disabledTextColor = TextStyle.Default.color
            ),
            modifier = Modifier
                .fillMaxWidth()
                .clickable { clickable() }
        )
    }
}

@Preview(showBackground = true)
@Composable
fun TaskDateTextFieldPreview() {
    TodoTheme {
        DateTextField(value = "", onValueChanged = {}) {

        }
    }
}