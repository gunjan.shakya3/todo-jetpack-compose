package com.example.todo.ui.screens

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarResult
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.todo.R
import com.example.todo.data.Todo
import com.example.todo.ui.components.TodoAppBar
import kotlinx.coroutines.launch

enum class TodoAppScreens(@StringRes val title: Int) {
    DASHBOARD(title = R.string.welcome), CREATE_NEW_TASK(title = R.string.create_new_task)
}

@Composable
fun TodoApp(
    dashboardViewModel: DashboardViewModel = viewModel(),
    navController: NavHostController = rememberNavController()
) {
    val backStackEntry by navController.currentBackStackEntryAsState()
    val currentScreen =
        TodoAppScreens.valueOf(backStackEntry?.destination?.route ?: TodoAppScreens.DASHBOARD.name)
    val snackBarHostState = remember { SnackbarHostState() }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            TodoAppBar(
                currentScreen = currentScreen,
                canNavigateBack = navController.previousBackStackEntry != null,
                navigateUp = { navController.navigateUp() }
            )
        },
        snackbarHost = { SnackbarHost(hostState = snackBarHostState) },
    ) { innerPadding ->
        NavHost(
            navController = navController,
            startDestination = TodoAppScreens.DASHBOARD.name,
            modifier = Modifier
                .fillMaxSize()
        ) {
            //Dashboard
            composable(route = TodoAppScreens.DASHBOARD.name) {
                val context = LocalContext.current
                val scope = rememberCoroutineScope()
                val todos = dashboardViewModel.getAllTodos().collectAsState(initial = emptyList())

                DashboardScreen(
                    modifier = Modifier
                        .padding(innerPadding)
                        .fillMaxSize(),
                    todos = todos.value,
                    onNewTaskButtonClick = { navController.navigate(TodoAppScreens.CREATE_NEW_TASK.name) },
                    onDoneTask = { todoId, isDone -> dashboardViewModel.doneTodo(todoId, isDone) }
                ) { todo: Todo ->
                    scope.launch {
                        val result = snackBarHostState.showSnackbar(
                            message = context.getString(
                                R.string.are_you_sure_you_want_to_delete,
                                todo.taskName
                            ),
                            actionLabel = context.getString(R.string.yes),
                            withDismissAction = true,
                            duration = SnackbarDuration.Indefinite
                        )
                        when (result) {
                            SnackbarResult.ActionPerformed -> dashboardViewModel.deleteTodo(
                                todoId = todo.id
                            )

                            SnackbarResult.Dismissed -> Unit
                        }
                    }
                }
            }

            //Add New Task
            composable(route = TodoAppScreens.CREATE_NEW_TASK.name) {
                CreateNewTaskScreen(
                    onCreateTask = {
                        dashboardViewModel.upsertTodo(it)
                        navController.navigateUp()
                    },
                    modifier = Modifier
                        .padding(innerPadding)
                        .fillMaxSize()
                )
            }
        }
    }
}