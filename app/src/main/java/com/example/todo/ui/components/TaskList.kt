package com.example.todo.ui.components

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.LocalOverscrollConfiguration
import androidx.compose.foundation.gestures.ScrollableDefaults
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.todo.data.DataSource
import com.example.todo.data.Todo
import com.example.todo.ui.theme.TodoTheme

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun TaskList(
    todoList: List<Todo>,
    onDoneTask: (Int, Boolean) -> Unit,
    onDeleteTask: (Todo) -> Unit
) {
    CompositionLocalProvider(value = LocalOverscrollConfiguration provides null) {
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            userScrollEnabled = true,
            reverseLayout = false,
            flingBehavior = ScrollableDefaults.flingBehavior(),
            contentPadding = PaddingValues(vertical = 8.dp)
        ) {
            items(todoList.size) { index ->
                val todo = todoList[index]
                TaskItem(
                    todo = todo,
                    onDoneTask = onDoneTask,
                    onDeleteTask = onDeleteTask
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun TaskListPreview() {
    TodoTheme {
        TaskList(/*Preview*/
            todoList = DataSource.todos,
            onDoneTask = { _, _ -> },
            onDeleteTask = {}
        )
    }
}