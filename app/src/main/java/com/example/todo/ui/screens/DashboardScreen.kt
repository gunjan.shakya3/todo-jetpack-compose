package com.example.todo.ui.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.todo.R
import com.example.todo.data.DataSource
import com.example.todo.data.Todo
import com.example.todo.ui.components.EmptyListContainer
import com.example.todo.ui.components.TaskList
import com.example.todo.ui.theme.TodoTheme

@Composable
fun DashboardScreen(
    modifier: Modifier = Modifier,
    todos: List<Todo>,
    onNewTaskButtonClick: () -> Unit,
    onDoneTask: (Int, Boolean) -> Unit,
    onDeleteTask: (Todo) -> Unit
) {
    Box {
        Column(
            modifier = modifier
                .padding(horizontal = 16.dp, vertical = 8.dp)
                .fillMaxSize()
        ) {
            Text(
                text = stringResource(id = R.string.your_tasks),
                style = TextStyle(
                    fontSize = 16.sp,
                    fontFamily = FontFamily.Serif,
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colorScheme.onBackground
                ),
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(16.dp))
            if (todos.isNotEmpty()) {
                TaskList(
                    todoList = todos,
                    onDoneTask = { todoId, isDone -> onDoneTask(todoId, isDone) }
                ) { onDeleteTask(it) }
            } else {
                EmptyListContainer(
                    modifier = Modifier.fillMaxSize(),
                    onCreateNewTask = onNewTaskButtonClick
                )
            }
        }

        FloatingActionButton(
            onClick = onNewTaskButtonClick,
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .padding(end = 16.dp, bottom = 32.dp)
        ) {
            Icon(
                imageVector = Icons.Filled.Add,
                contentDescription = stringResource(id = R.string.add_task),
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DashboardScreenPreview() {
    TodoTheme {
        DashboardScreen(
            todos = DataSource.todos,
            onNewTaskButtonClick = {},
            onDoneTask = { _, _ -> }
        ) {}
    }
}