package com.example.todo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.example.todo.data.TodoDatabase
import com.example.todo.ui.screens.DashboardViewModel
import com.example.todo.ui.screens.TodoApp
import com.example.todo.ui.theme.TodoTheme

class MainActivity : ComponentActivity() {
    private val database by lazy {
        Room
            .databaseBuilder(
                context = applicationContext,
                klass = TodoDatabase::class.java,
                name = "todo.db"
            )
            .fallbackToDestructiveMigration()
            .build()
    }

    private val dashboardViewModel by viewModels<DashboardViewModel>(
        factoryProducer = {
            object : ViewModelProvider.Factory {
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return DashboardViewModel(database.todoDAO) as T
                }
            }
        }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TodoTheme {
                TodoApp(dashboardViewModel = dashboardViewModel)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    TodoTheme {
        TodoApp()
    }
}