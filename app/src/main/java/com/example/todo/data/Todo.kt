package com.example.todo.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Todo(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val taskName: String,
    val taskDescription: String,
    val taskPriority: TaskPriority,
    val taskDate: String,
    val isDone: Boolean = false
)

enum class TaskPriority {
    Low, Medium, High
}
