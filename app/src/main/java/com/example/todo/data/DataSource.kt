package com.example.todo.data

object DataSource {
    /*val todos =  mutableListOf<Todo>()*/
    val todos = listOf(
        Todo(
            id = 1,
            taskName = "Testing",
            taskDescription = "This is for testing purpose",
            taskPriority = TaskPriority.Low,
            taskDate = "April 4th",
            isDone = true
        ),
        Todo(
            id = 2,
            taskName = "Do assignment",
            taskDescription = "This is for testing purpose",
            taskPriority = TaskPriority.Medium,
            taskDate = "May 10th",
            isDone = false
        ),
        Todo(
            id = 3,
            taskName = "Meeting with XYZ",
            taskPriority = TaskPriority.High,
            taskDescription = "This meeting is important",
            taskDate = "April 5th",
            isDone = false
        ),
        Todo(
            id = 4,
            taskName = "Upload a video",
            taskPriority = TaskPriority.Low,
            taskDescription = "Upload tutorial video on YouTube",
            taskDate = "May 8th",
            isDone = false
        ),
        Todo(
            id = 5,
            taskName = "Meeting with ABC",
            taskPriority = TaskPriority.High,
            taskDescription = "This is for testing purpose",
            taskDate = "April 4th",
            isDone = false
        )
    )
}