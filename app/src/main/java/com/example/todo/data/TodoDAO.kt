package com.example.todo.data

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import kotlinx.coroutines.flow.Flow

@Dao
interface TodoDAO {
    @Upsert
    suspend fun upsertTodo(todo: Todo)

    @Query("SELECT * FROM Todo")
    fun getAllTodos(): Flow<List<Todo>>

    @Query("UPDATE todo SET isDone = :isDone WHERE id = :todoId")
    fun doneTodo(todoId: Int, isDone: Boolean)

    @Query("SELECT * FROM Todo ORDER BY taskPriority = :priority")
    fun sortByPriority(priority: String): Flow<List<Todo>>

    @Query("DELETE FROM Todo WHERE id = :todoId")
    fun deleteTodo(todoId: Int)
}