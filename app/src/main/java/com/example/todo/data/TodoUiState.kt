package com.example.todo.data

data class TodoUiState(
    val isTodoListEmpty: Boolean = true,
    val todoList: List<Todo> = listOf()
)