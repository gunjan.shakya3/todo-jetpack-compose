# Todo App - Dashboard Screen

This document provides an overview of the screens of the Todo app, along with some screenshots for reference.

## Dashboard Screen

The Dashboard screen is the main screen of the Todo app, where users can view their tasks, add new tasks, mark tasks as completed, and perform other actions related to task management.

### Features

- View a list of tasks categorized by priority.
- Add new tasks with a title and description.
- Mark tasks as completed.
- Delete tasks individually.

### Main Dashboard

![Main Dashboard](screenshots/dashboard.jpg)

### Adding a New Task

![Adding a New Task](screenshots/new_task.jpg)